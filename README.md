# TensorFlowJS - Basic multi-class classifier demonstration

This is a repository contains a basic website that uses a tensorflow js model. The website loads the model using async and runs user-defined input through the predict-function of the model.

**The actual model files are not included and must be injected before usage.**

*The project eventually was re-based completely on the work by Mihail Salnikov at https://github.com/MihailSalnikov/NERjs.*


## Basic use (Getting Started)
```bash
# Run a basic node-powered webserver serving the folder with website and model
# Get node from https://nodejs.org/en/ or similar
npm install
node server.js
```

The website should now be available at `http://localhost:80/index.html`.
