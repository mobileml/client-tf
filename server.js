let serveIndex = require("serve-index");
let express = require("express");
let app = express();

const PORT = 80;

app.use(function(req, res, next) {
    console.log(`${new Date()} - ${req.method} request for ${req.url}`);
    next();    
});

app.use(express.static("./static"), serveIndex("./static"));

app.listen(PORT, function() {
    console.log("Serving static on http://localhost:" + PORT);
});