// For getting topics by topic ID
const getKey = (obj, val) => Object.keys(obj).find(key => obj[key] === val);

// equivalent of argsort
const dsu = (arr1, arr2) => arr1
  .map((item, index) => [arr2[index], item]) // add the args to sort by
  .sort(([arg1], [arg2]) => arg2 - arg1) // sort by the args
  .map(([, item]) => item); // extract the sorted items

// global variable to hold our model
let model;
const status = document.getElementById('status');

(async function() {
  const modelUrl = 'assets/model.json';
  status.textContent = `loading model from ${modelUrl} ...`;  
  model = await tf.loadLayersModel(modelUrl);
  status.textContent = `done loading model.`;  
})();


function cleanText(text) {
  text = text.toLowerCase();

  // remove hyperlinks
  text = text.replace(
    /(www|https?:\/\/)(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+/g,
    ""
  );

  // replace all punctuations with ' '
  text = text.replace(/['!"#$%&\\'()\*+,\-\.\/:;<=>?@\[\\\]\^_`{|}~']/g, " ");

  // replace all pure numbers with num
  text = text.replace(/\b\d+\b/g, "num");

  // remove all non-standard symbols/characters
  text = text.replace(/[^A-Za-z0-9,.?]/g, " ");

  // remove all single letters
  text = text.replace(/\b[\w]\b/g, "");

  // replace two or more spaces by a single space
  text = text.replace(/\s\s+/g, " ");

  // replace all digit sequences by a single digit
  text = text.replace(/\d+/g, "1");

  text = text.trim();

  return text;
};

function makeSequence(wordsArray, wordsVocab=WORDS_VOCAB) {
  let sequence = Array();
  wordsArray.slice(0, MAX_SEQUENCE_LENGTH).forEach(function(word) {
    let id = wordsVocab[word];
    if (id == undefined) {
      sequence.push(wordsVocab['<UNK>']);
    } else {
      sequence.push(id);
    }  
  });

  // pad sequence
  if (sequence.length < MAX_SEQUENCE_LENGTH) {
    let pad_array = Array(MAX_SEQUENCE_LENGTH - sequence.length);
    pad_array.fill(wordsVocab['<PAD>']);
    sequence = sequence.concat(pad_array);
  }

  return sequence;
};


function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

async function predictRandom() {
  const numExamples = Object.keys(EXAMPLE_DATA).length;
  const idx = getRandomInt(numExamples);
  $('#input_text').val(EXAMPLE_DATA[idx]);
  await predict();
}

async function predict() {
  const count = $('#input_topN').val();
  const input = $('#input_text').val();

  if (input === "") {
    alert("Please provide some input");
    return;
  }

  const t0 = tf.util.now();

  // Split input text to words
  let words = cleanText(input).split(' ');
  // Model works on sequences of MAX_SEQUENCE_LENGTH words, make one 
  let sequence = makeSequence(words);
  // convert JS array to TF tensor
  let tensor = tf.tensor1d(sequence, dtype='int32').expandDims(0);
  // run prediction, obtain a tensor
  let predictions = await model.predict(tensor);  
  // convert tensor to JS array
  predictions = await predictions.array();
  // we predicted just a single instance, get it's results
  predictions = predictions[0];
  
  const tElapsed = tf.util.now() - t0;
  status.textContent = `classification took ${tElapsed.toFixed(1)} ms`;

  const numClasses = Object.keys(TAGS_VOCAB).length;
  // array of sequential indices into predictions
  const indices = Array.from(Array(numClasses), (x, index) => index);

  // prediction indices sorted by score (probability)
  const sorted_indices = dsu(indices, predictions);
  const topN = sorted_indices.slice(0, count);
  
  const results = topN.map(function(tagId) {
    const topic = getKey(TAGS_VOCAB, tagId);
    const prob = predictions[tagId];
    return {
      Topic: topic,
      score: prob
    }
  });

  var table = new Tabulator(".main-result", {
    data: results,
    history: false,
    movableColumns: false,
    resizableRows: false,
    columns: [
      {title: "Topic", field: "Topic", headerSort: false},
      {title: "Score", field: "score", headerSort: false},
    ],
  });
};

$("#get_class_button").click(predict);
$('#input_text').keypress(function (e) {
  if (e.which == 13) {
    predict();
  }
});
$("#get_rnd_button").click(predictRandom);